package ru.market.service.impl;

import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import ru.market.entity.Product;
import ru.market.service.DataProviderService;

import java.util.Optional;

@Service
public class DataProviderServiceImpl implements DataProviderService {

    @Override
    @CachePut("products")
    public Optional<Product> getProductById(Long id) {
        //todo реализация получения данных о товаре из стороннего микросервиса
        return Optional.empty();
    }
}
