package ru.market.service;

import ru.market.entity.Product;

import java.util.Optional;

public interface DataProviderService {

    Optional<Product> getProductById(Long id);
}
