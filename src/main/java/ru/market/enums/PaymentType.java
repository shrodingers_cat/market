package ru.market.enums;

import lombok.Getter;

@Getter
public enum PaymentType {
    CREDIT_CARD,
    CASH_ON_DELIVERY,
    CASH_ON_PICKUP
}
