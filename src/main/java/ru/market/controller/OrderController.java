package ru.controller;

import org.springframework.web.bind.annotation.RequestBody;
import ru.market.controller.url.OrderUrlConstants;
import ru.market.dto.OrderCalculationResponse;
import ru.market.dto.OrderCalculationRequest;
import ru.market.dto.common.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.market.exception.WrongInputDataException;
import ru.market.service.OrderService;

import javax.validation.Valid;

@RestController
@RequestMapping(OrderUrlConstants.ROOT)
@Api(tags = "Контроллер для взаимодействия с заказами")
public class OrderController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "Рассчитать стоимость переданного заказа")
    @PostMapping(OrderUrlConstants.CALCULATE)
    public ApiResponse<OrderCalculationResponse> calculateOrder(@RequestBody @Valid OrderCalculationRequest request) throws WrongInputDataException {
        logger.info("Расчет стоимости переданного заказа");
        return ApiResponse.success(orderService.calculateOrder(request));
    }
}
