package ru.market.controller.url;

public final class OrderUrlConstants {

    private OrderUrlConstants() {
    }

    public static final String ROOT = "/v1/order";

    public static final String CALCULATE = "/calculate";
}
