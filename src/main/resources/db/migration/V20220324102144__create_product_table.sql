CREATE TABLE market.product
(
    id    BIGSERIAL PRIMARY KEY,
    price NUMERIC
);
