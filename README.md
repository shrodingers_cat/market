Сразу же изменил на свое усмотрение тип хранения id товара с int на long, первый, для подобных целей в своей практике никогда не использую. 
Хранение информации о товарах(сущность Product) представил в БД postgres используя ORM-маппинг. 
Дополнительно использовал кэш на уровне spring. Обращение к нему происходит при попытке получения сущности через абстракцию в виде репозитория(ProductRepository). 
Добавление новых сущностей - при получении из стороннего микросервиса(DataProviderService). 
Из потенциально спорных моментов, которые можно было бы решить альтернативным путем: обработка ситуации при невозможности найти товар по переданному на расчет стоимости запросу. Сейчас, в случае когда искомый товар не удается найти ни в БД, ни через сторонний микросервис, немедленно выбрасывается исключение. 
Из возможных вариантов развитие сервиса, можно выделить развитие функционала хранения и получения данных из альтернативного хранилища кэша(Redis). Расчет стоимости корзины учитывая тип оплаты и другие условия.
При наличии большего временного ресурса на данное задание, в первую очередь добавил бы поддержку кода в виде юнит-тестирования.
